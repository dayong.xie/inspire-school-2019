#!/bin/bash


# This script is part of the convenience build system for the IWR Dune course.
# It installs all the dune modules used throughout the course.
#
# The following environment variables are recognized:
# F77 the fortran compiler (gfortran)
# CC the C compiler (gcc)
# CXX the C++ compiler (g++)
# CXXFLAGS the standard C++ flags for external libraries ("-O3 -DNEBUG")
# CFLAGS the standard C flags for external libraries (copy CXXFLAGS)
# MAKE_FLAGS flags to be given to make during the build process ("-j2")

ROOT=$(pwd)
if [ ! "$F77" ]; then
  F77=gfortran
fi
if [ ! "$CC" ]; then
CC=gcc
fi
if [ ! "$CXX" ]; then
CXX=g++
fi
if [ ! "$CXXFLAGS" ]; then
CXXFLAGS="-O3 -DNDEBUG"
fi
CFLAGS="$CXXFLAGS"

# To avoid an ugly cmake bug we expand our compiler variables to absolute paths
export CC=$(which $CC)
export CXX=$(which $CXX)
export F77=$(which $F77)

# Check whether we have built external dependencies
if [ ! -d "$(pwd)/external/ug" ]; then
  ./external.sh
fi

# generate an opts file with releases flags
echo "
if [ -z \"\${CORES}\" ] ; then
  CORES=2
fi
MAKE_FLAGS=-j\${CORES}
CMAKE_FLAGS=\"
-DCMAKE_C_COMPILER='$CC'
-DCMAKE_CXX_COMPILER='$CXX'
-DCMAKE_CXX_FLAGS='-Wall -DDUNE_AVOID_CAPABILITIES_IS_PARALLEL_DEPRECATION_WARNING'
-DCMAKE_CXX_FLAGS_RELEASE='-O3 -DNDEBUG -g0 -funroll-loops'
-DDUNE_SYMLINK_TO_SOURCE_TREE=1
-DDUNE_SYMLINK_RELATIVE_LINKS=1
-DCMAKE_BUILD_TYPE=Release
-DDUNE_PYTHON_FORCE_PYTHON3=1
-DDUNE_PYTHON_ALLOW_GET_PIP=1
-DDUNE_PYTHON_VIRTUALENV_SETUP=1
\"" > release.opts

# generate an opts file with debug flags
echo "
if [ -z \"\${CORES}\" ] ; then
  CORES=2
fi
MAKE_FLAGS=-j${CORES}
CMAKE_FLAGS=\"
-DCMAKE_C_COMPILER='$CC'
-DCMAKE_CXX_COMPILER='$CXX'
-DCMAKE_CXX_FLAGS='-Wall -DDUNE_AVOID_CAPABILITIES_IS_PARALLEL_DEPRECATION_WARNING'
-DCMAKE_CXX_FLAGS_DEBUG='-O0 -ggdb'
-DDUNE_SYMLINK_TO_SOURCE_TREE=1
-DDUNE_SYMLINK_RELATIVE_LINKS=1
-DCMAKE_BUILD_TYPE=Debug
-DDUNE_PYTHON_FORCE_PYTHON3=1
-DDUNE_PYTHON_ALLOW_GET_PIP=1
-DDUNE_PYTHON_VIRTUALENV_SETUP=1
\"" > debug.opts

git submodule update --init --recursive

(
    # patch dune-codegen if necessary
    cd dune/dune-codegen
    if [[ ! -f ./.patches-applied ]] ; then
        echo "Patching dune-codegen submodules" 1>&2
        ./patches/apply_patches.sh || exit 1
        touch .patches-applied
    fi
)
if [[ $? -ne 0 ]] ; then
    echo "Failed to apply patches to dune-codegen submodules, bailing out" 1>&2
    exit 1
fi

rm -rf debug-build release-build

./dune/dune-common/bin/dunecontrol --opts=release.opts --builddir=$(pwd)/release-build --module=dune-pdelab-tutorials all

# This could create a debug build this way:
# ./dune/dune-common/bin/dunecontrol --opts=debug.opts --builddir=$(pwd)/debug-build --module=dune-pdelab-tutorials all

# disable for Norway
# cd doc
# latexmk -pdf structure.tex
# latexmk -pdf -c structure.tex
# cd ..
